## LOGIC CALCULATOR
To run the logic calculator (on Ubuntu):  
1. clone the repository: `$ git clone https://cmarilier@bitbucket.org/thesmallfactory/logic-calculator.git`  
2. open the directory (which has just been created) in a terminal  
3. install the logic calculator by Maven: `$ mvn clean install`  
4. for running the logic calculator, use the script: `$ ./logic-calculator`

#### *Nota bene*
To be able to use the script from anywhere, edit the file ".bashrc" and add the line:  
`export PATH="${PATH}:<logic-calculator_script_directory_path>"`

© Cyril Marilier, Nice, August 2018.
