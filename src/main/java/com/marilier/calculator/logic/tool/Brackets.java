package com.marilier.calculator.logic.tool;

/**
 * This {@code enum}(eration) allows to get the sign of each atom...
 * 
 * @author Cyril Marilier
 */
public enum Brackets {

	ROUND("(", ")"),
	CURLY("{", "}"),
	SQUARE("[", "]"); 

	private String opening;
	private String closing;

	private Brackets(String opening, String closing) {

		this.opening = opening;
		this.closing = closing;
	}

	public String getOpening() {

		return opening;
	}

	public String getClosing() {

		return closing;
	}

}
