package com.marilier.calculator.logic.tool;

import com.marilier.calculator.logic.entity.Regex;

/**
 * This class allows to have a <b>rich String</b>,
 * with (among others) the method "isWrapped()"...
 * 
 * @author Cyril Marilier
 */
public class RichString {

	private String string;
	private boolean stringIsRegex;

	private String o;
	private String c;

	public RichString(Object string) {

		this.string = string.toString();
		stringIsRegex = string instanceof Regex;
	}

	/**
	 * Removes each substring of this string that matches the literal target sequence.
	 * 
	 * @param target
	 * 
	 * @return the resulting string
	 */
	public String remove(CharSequence target) {

		return string.replace(target, "");
	}

	public String multipliedBy(int number) {

		String separation = "";
		for (int j = 0; j < number + 1; j++) {
			separation += string;
		}

		return separation;
	}

	public String wrap(Brackets brackets) {

		if (!(this.isWrappedIn(brackets))) {
			string = o + string + c;
		}

		if (stringIsRegex) {
			string = string.replace(o, "\\" + o).replace(c, "\\" + c);
		}

		return string;
	}

	public String unwrap(Brackets brackets) {

		if (this.isWrappedIn(brackets)) {
			string = string.substring(1, string.length() - 1);
		}

		return string;
	}

	public boolean isWrappedIn(Brackets brackets) {

		get(brackets);

		return (string.startsWith(o) && string.endsWith(c));
	}

	public String getContentWrappedIn(Brackets brackets) {

		get(brackets);

		return string.substring(string.indexOf(o) + 1, string.indexOf(c));
	}

	private void get(Brackets brackets) {

		o = brackets.getOpening();
		c = brackets.getClosing();
	}

	public String toString() {

		return string;
	}

}
