package com.marilier.calculator.logic.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.marilier.calculator.logic.sign.Connector;
import com.marilier.calculator.logic.sign.Designatable;

import com.marilier.calculator.logic.tool.Brackets;

/**
 * This {@code enum}(eration) allows to define the regular expressions which should be used as constant...
 * 
 * @author Cyril Marilier
 */
public enum Regex implements Designatable {

	// The regular expression to find the atoms...
	ATOM_REGEX("p" + "\\" + (Brackets.SQUARE).getOpening() + "\\d+" + "\\" + (Brackets.SQUARE).getClosing()),

	// The negation...
	NEGATION("¬"),
	// The regular expression to find the negations...
	NEGATION_REGEX(NEGATION + "*" + ATOM_REGEX.toString()),

	// The regular expression to find the connectors...
	CONNECTORS(getConnectorRegex()),
	// The regular expression to find the subformulas...
	PROPOSITION_REGEX(NEGATION_REGEX + CONNECTORS.toString() + NEGATION_REGEX);

	private String ex;

	private Regex(String ex) {

		this.ex = ex;
	}

	@Override
	public String toString() {

		return ex;
	}

	private static final String getConnectorRegex() {

		List<String> connectors = new ArrayList<>();
		(Arrays.asList(Connector.values())).forEach(c -> connectors.add(c.toString()));

		return "(" + String.join("|", connectors) + ")";
	}

}
