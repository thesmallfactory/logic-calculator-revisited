package com.marilier.calculator.logic.entity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This class allows to construct a subformulas level (as a map).
 * 
 * @author Cyril Marilier
 */
public class Subformulas extends LinkedHashMap<String, Subformulas.Subformula> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Subformulas() {

	}

	public Subformulas(Map<? extends String, ? extends Subformula> m) {

		super(m);
	}

	/**
	 * To replace the truthTable, if necessary,in the old value.
	 * 
	 * @param subformula
	 * @param truthTable
	 */
	public void put(String subformula, TruthTable truthTable) {

		Subformula sf = get(subformula);
		sf.truthTable = truthTable;
		put(subformula, sf);
	}

	/**
	 * To initialize a mapping for the specified key
	 * (by associating {@code null} with the specified key in the map)
	 * then increment the index by 1 and return it,
	 * if the map does not contain a mapping for the specified key.
	 * 
	 * @param subformula
	 * @param index
	 * 
	 * @return the index
	 */
	public int putIfAbsent(String subformula, int index) {

		if (!containsKey(subformula)) {
			put(subformula, new Subformula());

			index++;
		}

		return index;
	}

	public String getLastKey() {

		return String.valueOf((this.keySet()).toArray()[this.size() - 1]);
	}

	public static class Subformula {

		private String content;
		private TruthTable truthTable;

		public Subformula(String content, TruthTable truthTable) {

			this.content = content;
			this.truthTable = truthTable;
		}

		public Subformula() {

		}

		public String getContent() {

			return content;
		}

		public TruthTable getTruthTable() {

			return truthTable;
		}

	}

	/**
	 * This internal class allows to have the truth table (of any subformula)
	 * as an object (of type {@code TruthTable}).
	 */
	public static class TruthTable extends ArrayList<Boolean> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TruthTable() {

		}

		public TruthTable(List<? extends Boolean> l) {

			super(l);
		}

		public String toString() {

			String truths = "";

			for (Boolean truth : this) {
				truths += (truth ? "1" : "0");
			}

			return truths;
		}

	}

}
