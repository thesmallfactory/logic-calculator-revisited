package com.marilier.calculator.logic;

import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import java.util.function.BiFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.stream.Collectors;

import com.marilier.calculator.logic.entity.Regex;
import com.marilier.calculator.logic.entity.Subformulas;
import com.marilier.calculator.logic.entity.Subformulas.Subformula;
import com.marilier.calculator.logic.entity.Subformulas.TruthTable;

import com.marilier.calculator.logic.exception.NotFormulaException;

import com.marilier.calculator.logic.sign.Atom;
import com.marilier.calculator.logic.sign.Connector;

import com.marilier.calculator.logic.tool.RichString;
import com.marilier.calculator.logic.tool.Brackets;

/**
 * This class allows to calculate the truth table of an expression (which is a formula).
 * 
 * @author Cyril Marilier
 */
public class Calculator {

	// The atoms
	private Subformulas atoms;
	// The tree of decomposition
	private List<Subformulas> decompositionTree;

	// The subformulas
	private Subformulas subformulas;


	// Index...
	private int i = 0;
	private int j;

	// The atomic number (i.e. the number of the "highest" atom;
	// for example, let e be the expression "p∧r", then the "highest" atom is "r"...)
	private int atomicNumber;

	// The number of cases (i.e. the number of lines of the truth table)
	private int casesNumber;


	private String name;
	private String expression;

	public Calculator(String name, String expression) {

		this.name = name;
		this.expression = expression;
		// Prints the expression (with its name)
		System.out.println("0 - " + this.name + ":\n\t" + this.expression);

		// Transforms the expression in order to be able to decompose it...
		expression = initialize(expression);

		// PERFORMS THE SYNTACTIC ANALYSIS:
		// Decomposes the expression in order to be able to calculate it...
		try {
			expression = decompose(expression);
		} catch (NotFormulaException nfe) {
			// (If the expression is not a formula...)
			nfe.printStackTrace();
			System.exit(0);
		}
		casesNumber = twoPower(atomicNumber);
		// PERFORMS THE SEMANTIC ANALYSIS, IF (AND ONLY IF) THE EXPRESSION IS A FORMULA:
		// If (and only if) the expression is a formula, calculates it...
		calculate(expression);

		// Prints the truth table (of the expression)
		printTruthTable();
	}

	/**
	 * To get the subformulas for <i>testing</i>...
	 * 
	 * @return subformulas
	 */
	public Subformulas getSubformulas() {

		return subformulas;
	}

	public void printTruthTable() {

		System.out.println("1 - Truth table:");
		System.out.println("      ┌" + (new RichString("─")).multipliedBy(32 + (casesNumber + 1)) + "┐");
		String[] subformulaNames = (subformulas.keySet()).toArray(new String[0]);
		for (int i = 0; i < subformulaNames.length; i++) {
			String subformulaName = subformulaNames[i];
			Subformula subformula = subformulas.get(subformulaName);
			String subformulaContent = (new RichString(subformula.getContent())).wrap(Brackets.ROUND) + ":";

			TruthTable truthTable = subformula.getTruthTable();
			String printingFormat = "      │ " + subformulaName + "\t" + subformulaContent + "\t%s" + truthTable + " │";

			// If the expression is a tautology, saves it (in a text file)...
			if (i == subformulaNames.length - 1 && !((truthTable.toString()).contains("0"))) {
				try {
					Path path = Paths.get("Tautologies.txt");

					if (Files.notExists(path)) {
						Files.createFile(path);
					}

					Charset charset = StandardCharsets.UTF_8;

					List<String> tautologies = Files.readAllLines(path, charset);
					if (!(tautologies.contains(expression))) {
						tautologies.add(expression);
					}
					Files.write(path, tautologies,  charset);
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}

			int subformulaNumber = Integer.parseInt((new RichString(subformulaName)).getContentWrappedIn(Brackets.SQUARE));
			if (subformulaNumber <= atomicNumber) {
				System.out.println(String.format(printingFormat, "\t\t"));
				if (subformulaNumber == atomicNumber) {
					System.out.println("      ╞" + (new RichString("═")).multipliedBy(32 + (casesNumber + 1)) + "╡");
				}
			} else {
				System.out.println(String.format(printingFormat, (subformulaContent.length() < 16) ? "\t" : ""));
			}
		}
		System.out.println("      └" + (new RichString("─")).multipliedBy(32 + (casesNumber + 1)) + "┘");
		System.out.println();
	}


	public static void main(String[] args) throws InterruptedException {

		System.out.println();
		System.out.println("┌─┐    ┌─────┐┌─────┐┌─┐┌─────┐");
		System.out.println("│ │    │ ┌─┐ ││ ┌───┘│ ││ ┌───┘");
		System.out.println("│ │    │ │ │ ││ │    │ ││ │");
		System.out.println("│ │    │ │ │ ││ │ ┌─┐│ ││ │");
		System.out.println("│ └───┐│ └─┘ ││ └─┘ ││ ││ └───┐");
		System.out.println("└─────┘└─────┘└─────┘└─┘└─────┘");
		System.out.println("       ┌─────┐┌─────┐┌─┐    ┌─────┐┌─┐ ┌─┐┌─┐    ┌─────┐┌─────┐┌─────┐┌──────┐");
		System.out.println("       │ ┌───┘│ ┌─┐ ││ │    │ ┌───┘│ │ │ ││ │    │ ┌─┐ │└─┐ ┌─┘│ ┌─┐ ││ ┌──┐ │");
		System.out.println("       │ │    │ └─┘ ││ │    │ │    │ │ │ ││ │    │ └─┘ │  │ │  │ │ │ ││ └──┘ │");
		System.out.println("       │ │    │ ┌─┐ ││ │    │ │    │ │ │ ││ │    │ ┌─┐ │  │ │  │ │ │ ││ ┌─┐ ┌┘");
		System.out.println("       │ └───┐│ │ │ ││ └───┐│ └───┐│ └─┘ ││ └───┐│ │ │ │  │ │  │ └─┘ ││ │ │ └┐");
		System.out.println("       └─────┘└─┘ └─┘└─────┘└─────┘└─────┘└─────┘└─┘ └─┘  └─┘  └─────┘└─┘ └──┘");
		System.out.println("                                          © Cyril Marilier, Nice, August 2018.\n");

		String name = "Let the expression be";
		String expression;

		switch (args.length) {
			case 0:
				new Calculator("Destructive dilemma", "(((p⊃q)∧(r⊃s))∧(¬q∨¬s))⊃(¬p∨¬r)");
				Thread.sleep(10_000);
				System.out.println("The destructive dilemma is a default example...\n");
				System.out.println("You can enter an expression (as argument of the command),\nby using the numeric keypad as follows:");
				System.out.println("   ┌─────────────────────────────────┐");
				System.out.println("   │ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ │");
				System.out.println("   │ │Num  │ │≡    │ │w    │ │⊃    │ │");
				System.out.println("   │ │Lock │ │     │ │     │ │     │ │");
				System.out.println("   │ └─────┘ └─────┘ └─────┘ └─────┘ │");
				System.out.println("   │ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ │");
				System.out.println("   │ │7    │ │8    │ │9    │ │∨    │ │");
				System.out.println("   │ │Home │ │↑    │ │Pg Up│ │     │ │");
				System.out.println("   │ └─────┘ └─────┘ └─────┘ │     │ │");
				System.out.println("   │ ┌─────┐ ┌─────┐ ┌─────┐ │     │ │");
				System.out.println("   │ │4    │ │5    │ │6    │ │     │ │");
				System.out.println("   │ │←    │ │     │ │→    │ │     │ │");
				System.out.println("   │ └─────┘ └─────┘ └─────┘ └─────┘ │");
				System.out.println("   │ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ │");
				System.out.println("   │ │1    │ │2    │ │3    │ │Enter│ │");
				System.out.println("   │ │End  │ │↓    │ │Pg Dn│ │     │ │");
				System.out.println("   │ └─────┘ └─────┘ └─────┘ │     │ │");
				System.out.println("   │ ┌─────────────┐ ┌─────┐ │     │ │");
				System.out.println("   │ │¬            │ │∧    │ │     │ │");
				System.out.println("   │ │Ins          │ │Del  │ │     │ │");
				System.out.println("   │ └─────────────┘ └─────┘ └─────┘ │");
				System.out.println("   └─────────────────────────────────┘");
				System.out.println("So use:");
				System.out.println(" • the key \".\" for entering \"∧\",");
				System.out.println(" • the key \"+\" for entering \"∨\",");
				System.out.println(" • the key \"-\" for entering \"⊃\",");
				System.out.println(" • the key \"*\" for entering \"w\",");
				System.out.println(" • the key \"/\" for entering \"≡\",");
				System.out.println(" • the key \"0\" for entering \"¬\"...");
				System.out.println("┌─────────────────────────────────────────────────────────────────────────────────────────────┐");
				System.out.println("│ INFO: To correctly enter the expression, use a pair of double quotation marks; for example: │");
				System.out.println("│       $ java -jar logic-calculator-0.0.1-SNAPSHOT-jar-with-dependencies.jar \"¬(p.¬p)\"       │");
				System.out.println("└─────────────────────────────────────────────────────────────────────────────────────────────┘");
				break;
			case 1:
				expression = (((((args[0].replace(".", "∧")).replace("+", "∨")).replace("-", "⊃")).replace("*", "w")).replace("/", "≡")).replace("0", "¬");
				new Calculator(name, expression);
				break;
			default:
				// At most 1 expected argument: the expression to calculate...
				System.out.println("Use: java -jar logic-calculator.jar <expression>");
		}

	}

	/**
	 * To initialize the formula,
	 * by replacing each atom by a term of the form "p[<i>{@code n}</i>]"
	 * (where <i>{@code n}</i> is a <b>natural</b> integer)...
	 * 
	 * @param expression
	 *        before...
	 * 
	 * @return <b>expression</b>
	 *         after...
	 */
	private String initialize(String expression) {

		// Initialize the atoms
		atoms = new Subformulas();

		for (Atom a : Atom.values()) {
			String atom = a.toString();

			if (expression.contains(atom)) {
				atoms.put(p(i = a.ordinal() + 1), new Subformula(atom, null));
				expression = expression.replace(atom, p(j = i));
			}
		}

		atomicNumber = i;

		return expression;
	}

	/**
	 * To "reduce" (i.e. resolve) the formula, by decomposing it...
	 * (THE 1st ANALYSIS)
	 * 
	 * @param ex
	 *        before...
	 * 
	 * @return <b>expression</b>
	 *         after...
	 * 
	 * @throws NotFormulaException
	 */
	private String decompose(String ex) throws NotFormulaException {

		// Initialize the decomposition tree
		decompositionTree = new ArrayList<>();

		String expression = "";

		do {
			ex = (new RichString(ex)).wrap(Brackets.ROUND);

			Subformulas decompositionLevelSubformulas = new Subformulas();

			Matcher matcher = (Pattern.compile((Regex.PROPOSITION_REGEX).toString())).matcher(ex);
			while (matcher.find()) {
				String subformula = matcher.group();

				if (!(decompositionLevelSubformulas.containsKey(subformula))) {
					decompositionLevelSubformulas.put(subformula, new Subformula());
					ex = ex.replace("(" + subformula + ")", p(++i));
				}
			}
			decompositionTree.add(decompositionLevelSubformulas);

		} while (!(expression.equals(expression = ex)));

		// Throws a NotFormulaException if the expression is not a formula...
		if (!(expression.matches((new RichString(Regex.NEGATION_REGEX)).wrap(Brackets.ROUND)))) {
			throw new NotFormulaException();
		} else {
			return expression;
		}
	}

	/**
	 * To calculate the truth table of the formula.
	 * (THE 2nd ANALYSIS)
	 * 
	 * @param expression
	 */
	private void calculate(String expression) {

		// Initialize the subformulas
		subformulas = new Subformulas();

		// Calculates the truth table of each atom...
		(atoms.keySet()).forEach(atom -> {
			int atomNumber = Integer.parseInt((new RichString(atom)).getContentWrappedIn(Brackets.SQUARE));
			int changesNumber = twoPower(atomNumber);
			subformulas.put(atom, new Subformula((atoms.get(atom)).getContent(), atomTruthTable(changesNumber)));
		});

		// Calculates the truth table of each subformula...
		decompositionTree.forEach(decompositionLevel ->
			(decompositionLevel.keySet()).forEach(subformula -> {
				List<String> connection = Collections
						.list(new StringTokenizer(subformula, (Regex.CONNECTORS).toString(), true))
						.stream()
						.map(token -> token.toString())
						.collect(Collectors.toList());

				subformulas.put(p(++j), new Subformula(subformula, subformulaTruthTable(connection)));
			})
		);

		// (If the formula is a negation, recalculates its truth table...)
		TruthTable formulaTruthTable = negationTruthTable(expression);
		if (!(formulaTruthTable.equals((subformulas.get(p(j))).getTruthTable()))) {
			String formulaContent = Regex.NEGATION + p(j++);
			subformulas.put(p(j), new Subformula(formulaContent, formulaTruthTable));
		}
	}

	/**
	 * To define the truth table of an atom.
	 * 
	 * @param changesNumber
	 *        the number of changes of the truth value...
	 * 
	 * @return the truth table of the atom in question
	 */
	private TruthTable atomTruthTable(int changesNumber) {

		TruthTable atomTruthTable = new TruthTable();

		Boolean truthValue = false;
		for (int i = 0; i < casesNumber; i++) {
			if ((i != 0) && ((i % (casesNumber/changesNumber)) == 0)) {
				truthValue = !truthValue;
			}
			atomTruthTable.add(truthValue);
		}

		return atomTruthTable;
	}

	/**
	 * To calculate the truth table of a subformula.
	 * 
	 * @param connection
	 * 
	 * @return the truth table of the subformula in question
	 */
	private TruthTable subformulaTruthTable(List<String> connection) {

		TruthTable subformulaTruthTable = new TruthTable();

		TruthTable leftTermTruthTable = negationTruthTable(connection.get(0));
		BiFunction<Boolean, Boolean, Boolean> operator = Connector.getOperator(connection.get(1));
		TruthTable rightTermTruthTable = negationTruthTable(connection.get(2));
		for (int i = 0; i < casesNumber; i++) {
			subformulaTruthTable.add(operator.apply(leftTermTruthTable.get(i), rightTermTruthTable.get(i)));
		}

		return subformulaTruthTable;
	}

	/**
	 * If a subformula is a negation, to recalculate its truth table.
	 * 
	 * @param subformulaName
	 * 
	 * @return the truth table of the subformula in question...
	 */
	private TruthTable negationTruthTable(String subformulaName) {

		TruthTable newTruthTable = new TruthTable();

		int absurditiesNumber = 0;
		Matcher matcher = (Pattern.compile((Regex.NEGATION).toString())).matcher(subformulaName);
		while (matcher.find()) {
			absurditiesNumber++;
		}
		TruthTable oldTruthTable = subformulas.get((new RichString(((new RichString(subformulaName)).unwrap(Brackets.ROUND))).remove((Regex.NEGATION).toString()))).getTruthTable();
		if ((absurditiesNumber % 2) == 1) {
			for (Boolean oldTruthValue : oldTruthTable) {
				newTruthTable.add(!oldTruthValue);
			}
		} else {
			newTruthTable.addAll(oldTruthTable);
		}

		return newTruthTable;
	}

	/**
	 * To get the name of an "atom"...
	 * 
	 * @param i
	 * 
	 * @return the name of the "atom"
	 */
	private static String p(int i) {

		return String.format(((new RichString(Regex.ATOM_REGEX)).remove("\\")).replace("d+", "%d"), i);
	}

	/**
	 * To calculate two to the power of an exponent...
	 * 
	 * @param exponent
	 * 
	 * @return the power...
	 */
	private static int twoPower(int exponent) {

		return (Double.valueOf(Math.pow(2, exponent)).intValue());
	}

}
