package com.marilier.calculator.logic.sign;

/**
 * This {@code enum}(eration) allows to get the sign of each atom...
 * 
 * @author Cyril Marilier
 */
public enum Atom implements Designatable {

	P("p"),
	Q("q"),
	R("r"),
	S("s"),
	T("t");

	private String sign;

	private Atom(String sign) {

		this.sign = sign;
	}

	@Override
	public String toString() {

		return sign;
	}

}
