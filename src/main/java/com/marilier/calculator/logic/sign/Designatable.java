package com.marilier.calculator.logic.sign;

/**
 * The method declared in this interface allows the constants of an {@code enum}
 * (which implements this interface) to be designatable.
 * 
 * @author Cyril Marilier
 * 
 * @see Atom
 * @see Connector
 * 
 * @see Regex
 */
public interface Designatable {

	public String toString();
}
