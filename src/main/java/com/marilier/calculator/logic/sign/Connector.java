package com.marilier.calculator.logic.sign;

import java.util.function.BiFunction;

/**
 * This {@code enum}(eration) allows to get the sign, among other things, of each connector...
 * 
 * @author Cyril Marilier
 */
public enum Connector implements Designatable {

	CONJUNCTION("∧", (p, q) -> (p & q)),
	INCLUSIVE_DISJUNCTION("∨", (p, q) -> (p | q)), MATERIAL_IMPLICATION("⊃", (p, q) -> (!p | q)),
	EXCLUSIVE_DISJUNCTION("w", (p, q) -> (p ^ q)), EQUIVALENCE("≡", (p, q) -> (!p ^ q));

	private String sign;
	private BiFunction<Boolean, Boolean, Boolean> operator;

	private Connector(String sign, BiFunction<Boolean, Boolean, Boolean> operator) {

		this.sign = sign;
		this.operator = operator;
	}

	@Override
	public String toString() {

		return sign;
	}

	public static BiFunction<Boolean, Boolean, Boolean> getOperator(String sign) {

		BiFunction<Boolean, Boolean, Boolean> operator = null;

		for (Connector connector : Connector.values()) {
			if ((connector.toString()).equals(sign)) {
				operator = connector.getOperator();
			}
		}

		return operator;
	}

	public BiFunction<Boolean, Boolean, Boolean> getOperator() {

		return operator;
	}

}
