package com.marilier.calculator.logic.exception;

/**
 * This class allows to define a {@code NotFormulaException}.
 * 
 * @author Cyril Marilier
 */
public class NotFormulaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFormulaException() {

		super("The expression is not a formula.");
	}

}
