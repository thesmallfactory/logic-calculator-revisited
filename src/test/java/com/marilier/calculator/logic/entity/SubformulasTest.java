package com.marilier.calculator.logic.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.marilier.calculator.logic.entity.Subformulas;
import com.marilier.calculator.logic.entity.Subformulas.Subformula;
import com.marilier.calculator.logic.entity.Subformulas.TruthTable;

/**
 * This class allows to test some methods which are defined in the class "Subformulas".
 * 
 * @author Cyril Marilier
 */
public class SubformulasTest {

	private static final String ATOM = "p";
	private static final String NEGATION = "¬p";
	private static final String CONJUNCTION = "p∧q";
	private static final String DISJUNCTION = "p∨q";

	// The truth table of the ATOM "p"...
	private static final TruthTable ATOM_TRUTH_TABLE = new TruthTable();
	static {
		ATOM_TRUTH_TABLE.add(true);
		ATOM_TRUTH_TABLE.add(false);
	}
	// The truth table of the NEGATION of (the atom) "p"...
	private static final TruthTable NEGATION_TRUTH_TABLE = new TruthTable();
	static {
		NEGATION_TRUTH_TABLE.add(false);
		NEGATION_TRUTH_TABLE.add(true);
	}
	// The truth table of the CONJUNCTION
	private static final TruthTable CONJUNCTION_TRUTH_TABLE = new TruthTable();
	static {
		CONJUNCTION_TRUTH_TABLE.add(true);
		CONJUNCTION_TRUTH_TABLE.add(false);
		CONJUNCTION_TRUTH_TABLE.add(false);
		CONJUNCTION_TRUTH_TABLE.add(false);
	}
	// The truth table of the DISJUNCTION
	private static final TruthTable DISJUNCTION_TRUTH_TABLE = new TruthTable();
	static {
		DISJUNCTION_TRUTH_TABLE.add(true);
		DISJUNCTION_TRUTH_TABLE.add(true);
		DISJUNCTION_TRUTH_TABLE.add(true);
		DISJUNCTION_TRUTH_TABLE.add(false);
	}

	private Subformulas subformulasTest = new Subformulas();

	private int index;

	@Before
	public void initialize() {

		subformulasTest.put(ATOM, new Subformula(ATOM, ATOM_TRUTH_TABLE));
		subformulasTest.put(NEGATION, new Subformula(NEGATION, NEGATION_TRUTH_TABLE));
		subformulasTest.put(CONJUNCTION, new Subformula(CONJUNCTION, CONJUNCTION_TRUTH_TABLE));

		index = 11;
	}

	@Test
	public final void putIfAbsentTest() {

		Assert.assertEquals(12, subformulasTest.putIfAbsent(DISJUNCTION, index));
	}

	@Test
	public final void getLastKeyTest() {

		Assert.assertEquals(CONJUNCTION, subformulasTest.getLastKey());
	}

}
