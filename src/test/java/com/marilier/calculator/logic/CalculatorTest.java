package com.marilier.calculator.logic;

import org.junit.Assert;
import org.junit.Test;

import com.marilier.calculator.logic.Calculator;

import com.marilier.calculator.logic.entity.Subformulas;

/**
 * This class allows to test some methods which are defined in the class "Calculator".
 * 
 * @author Cyril Marilier
 */
public class CalculatorTest {

	@Test
	public final void CalculatorConjunctionTest() {

		String name = "The conjunction";
		String formula = "p∧q";

		Assert.assertEquals("0001", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorInclusiveDisjunctionTest() {

		String name = "The inclusive disjunction";
		String formula = "p∨q";

		Assert.assertEquals("0111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorMaterialImplicationTest() {

		String name = "The material implication";
		String formula = "p⊃q";

		Assert.assertEquals("1101", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorExclusiveDisjunctionTest() {

		String name = "The exclusive disjunction";
		String formula = "pwq";

		Assert.assertEquals("0110", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorEquivalenceTest() {

		String name = "The equivalence";
		String formula = "p≡q";

		Assert.assertEquals("1001", getTruthTable(name, formula));
	}

	/*
	 * Some tautologies...
	 */
	@Test
	public final void CalculatorLawOfExcludedMiddleTest() {

		String name = "Law of excluded middle";
		String formula = "¬p∨p";

		Assert.assertEquals("11", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorLawOfNoncontradictionTest() {

		String name = "Law of noncontradiction";
		String formula = "¬(p∧¬p)";

		Assert.assertEquals("11", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorLawOfDeMorganOneTest() {

		String name = "Law of De Morgan (from disjunction to conjunction)";
		String formula = "¬(p∨q)≡(¬p∧¬q)";

		Assert.assertEquals("1111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorLawOfDeMorganTwoTest() {

		String name = "Law of De Morgan (from conjunction to disjunction)";
		String formula = "¬(p∧q)≡(¬p∨¬q)";

		Assert.assertEquals("1111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorModusPonendoPonensTest() {

		String name = "Modus ponendo ponens";
		String formula = "((p⊃q)∧p)⊃q";

		Assert.assertEquals("1111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorModusTollendoTollensTest() {

		String name = "Modus tollendo tollens";
		String formula = "((p⊃q)∧¬q)⊃¬p";

		Assert.assertEquals("1111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorDoubleNegationTest() {

		String name = "Double negation";
		String formula = "¬¬p≡p";

		Assert.assertEquals("11", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorLawOfPeirceTwoTest() {

		String name = "Law of Peirce";
		String formula = "((p⊃q)⊃p)⊃p";

		Assert.assertEquals("1111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorAssociativityOfTheExclusiveDisjunctionTest() {

		String name = "Associativity of the exclusive disjunction";
		String formula = "((pwq)wr)≡(pw(qwr))";

		Assert.assertEquals("11111111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorExportationTest() {

		String name = "Exportation";
		String formula = "((p∧q)⊃r)⊃(p⊃(q⊃r))";

		Assert.assertEquals("11111111", getTruthTable(name, formula));
	}

	@Test
	public final void CalculatorImportationTest() {

		String name = "Importation";
		String formula = "(p⊃(q⊃r))⊃((p∧q)⊃r)";

		Assert.assertEquals("11111111", getTruthTable(name, formula));
	}


	private String getTruthTable(String name, String formula) {

		Subformulas subformulas = (new Calculator(name, formula)).getSubformulas();

		return ((subformulas.get(subformulas.getLastKey())).getTruthTable()).toString();
	}

}
